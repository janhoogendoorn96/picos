#!/usr/bin/python

# ------------------------------------------------------------------------------
# Copyright (C) 2018, 2020 Maximilian Stahlberg
#
# This file is part of PICOS Release Scripts.
#
# PICOS Release Scripts are free software: you can redistribute it and/or modify
# them under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PICOS Release Scripts are distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

"""Produce version strings for PICOS."""

import datetime
import os
import re
from subprocess import check_output

LOCATION = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
VERSION_FILE   = os.path.join(LOCATION, "picos", ".version")
CHANGELOG_FILE = os.path.join(LOCATION, "CHANGELOG.rst")

# Version lengthes.
SHORT           = 0
MEDIUM          = 1
MEDIUM_OR_SHORT = 2
LONG            = 3


def repository_is_dirty():
    """Report whether the repository has uncomitted changed."""
    description = check_output(["git", "describe", "--dirty"]).decode("ascii")
    return "dirty" in description


def _git_describe(tokens=True):
    with open(os.devnull, "w") as DEVNULL:
        gitVer = check_output(["git", "describe", "--long", "--match", "v*"],
            stderr=DEVNULL).decode("ascii").strip()
    return gitVer.split("-") if tokens else gitVer


def verify_version_file(raiseException=True, skipIfNoGit=False):
    """Report whether the version file matches the git version."""
    with open(VERSION_FILE, "r") as versionFile:
        fileVersion = versionFile.read().strip()

    try:
        gitVersion  = _git_describe()[0].lstrip("v")
    except Exception:
        if skipIfNoGit:
            gitVersion = fileVersion
        else:
            raise

    valid = fileVersion == gitVersion

    if raiseException:
        if not valid:
            raise Exception("File version is {} while git version is {}."
                .format(fileVersion, gitVersion))
    else:
        return valid


def get_base_version():
    """Report the version stored in the version file."""
    verify_version_file(skipIfNoGit=True)

    with open(VERSION_FILE, "r") as versionFile:
        version = versionFile.read().strip()

    return version


def get_commit_count():
    """Report number of commits since the last release."""
    gitVer = _git_describe()

    if len(gitVer) >= 2:
        return int(gitVer[1])
    else:
        raise Exception(
            "Failed to retrieve number of commits since the last release.")


def get_commit_hash():
    """Report the hash of git's HEAD commit."""
    gitVer = _git_describe()

    if len(gitVer) >= 3:
        return gitVer[2]
    else:
        raise Exception("Failed to retrieve the commit hash.")


def get_version(length=MEDIUM_OR_SHORT, patchPrefix=".", hashPrefix="+"):
    """Report the version as a string."""
    version = get_base_version()

    if length != SHORT:
        try:
            version = "{}{}{}".format(version, patchPrefix, get_commit_count())
        except Exception:
            # Fall back to SHORT if MEDIUM_OR_SHORT is desired.
            if length != MEDIUM_OR_SHORT:
                raise

    if length is LONG:
        version = "{}{}{}".format(version, hashPrefix, get_commit_hash())

    return version


def get_version_info():
    """Report the version in the format of Python's __version_info__ tuple."""
    return tuple(get_version().split("."))


def _bump_changelog(baseVersion):
    with open(CHANGELOG_FILE, "r") as logFile:
        log = logFile.read()

    log, count = re.subn(r".. _Unreleased: (.*)master", r".. _{0}: \g<1>v{0}"
        .format(baseVersion), log)

    if count != 1:
        raise Exception(
            "There is no 'Unreleased' link definition in the changelog.")

    log, count = re.subn(r"`Unreleased`_", "`{}`_ - {}"
        .format(baseVersion, datetime.date.today()), log)

    if count != 1:
        raise Exception(
            "There is no 'Unreleased' header in the changelog.")

    with open(CHANGELOG_FILE, "w") as logFile:
        logFile.write(log)


def bump_version(*parts):
    """Prepare the release of a new version."""
    for number in parts:
        if type(number) is not int:
            raise TypeError("Version number parts must be int.")

    if repository_is_dirty():
        raise Exception("You can only bump version in a non-dirty repository.")

    version = ".".join(["{}".format(x) for x in parts])
    if len(parts) == 2:
        major, minor = parts
    else:
        raise Exception("Version format must be MAJOR.MINOR.")

    oldVersion = get_base_version()
    oldParts = [int(x) for x in oldVersion.split(".")]
    if len(oldParts) == 2:
        oldMajor, oldMinor = oldParts
    elif len(oldParts) == 3:
        oldMajor, oldMinor, _ = oldParts  # Be compatible with old format.
    else:
        raise Exception("The old version format is invalid.")

    notNewer = False
    if major < oldMajor:
        notNewer = True
    elif major == oldMajor:
        if minor < oldMinor:
            notNewer = True

    if notNewer:
        raise Exception("The proposed version of {} is not newer than the "
            "current version of {}.".format(version, oldVersion))

    print("Relabling the 'Unreleased' section in the changelog.")
    _bump_changelog(version)

    print("Writing to version file.")
    with open(VERSION_FILE, "w") as versionFile:
        versionFile.write("{}\n".format(version))

    print("Commiting to git.")
    check_output(["git", "add", VERSION_FILE, CHANGELOG_FILE])
    check_output([
        "git", "commit", "-m", "Bump version to {}.".format(version)])

    print("The following commit has been made:")
    print("-"*35)
    print(check_output(["git", "show", "-U0"]).decode("ascii").strip())
    print("-"*35)

    print("Creating an annotated git tag.")
    check_output(["git", "tag", "-a", "v{}".format(version),
        "-m", "Release of version {}.".format(version)])

    print("Verifying that version file and git tag version match.")
    verify_version_file()

    print("\nAll set. Execute 'git push --follow-tags' to push the release!")


if __name__ == "__main__":
    import argparse

    def version(str):
        """Load a parameter as a version tuple."""
        return tuple(int(x) for x in str.lstrip("v").split("."))

    parser = argparse.ArgumentParser(description="PICOS version manager. "
        "When making a proper release, merge into 'master', then bump the "
        "version with this script BEFORE pushing to 'origin/master' "
        "(with --follow-tags).")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-t", "--test", action="store_true",
        help="verify that file and git versions match")
    group.add_argument("-b", "--bump", metavar="VER", type=version,
        help="bump version to VER = MAJOR.MINOR")
    group.add_argument("-s", "--short", action="store_true",
        help="print base version as MAJOR.MINOR")
    group.add_argument("-m", "--medium", action="store_true",
        help="print full version as MAJOR.MINOR.PATCH")
    group.add_argument("-f", "--flexible", action="store_true",
        help="like --medium if possible, else like --short (default)")
    group.add_argument("-l", "--long", action="store_true",
        help="print an extended full version with the commit hash")
    group.add_argument("-c", "--commit", action="store_true",
        help="print the current commit short hash")
    group.add_argument("--aur", action="store_true",
        help="same as --long -r '.r' -a '.'")
    group.add_argument("--pep", action="store_true",
        help="same as --flexible -r '.post'")

    parser.add_argument("-p", "--prefix", metavar="STR", type=str,
        default="", help="prefix for the base version")
    parser.add_argument("-r", "--patch-prefix", metavar="STR", type=str,
        default=".", help="prefix for PATCH (or revision) part")
    parser.add_argument("-a", "--hash-prefix", metavar="STR", type=str,
        default="+", help="prefix for the commit hash")

    args = parser.parse_args()

    if args.aur:
        args.short, args.flexible, args.medium, args.long = \
            False, False, False, True
        args.patch_prefix = ".r"
        args.hash_prefix = "."
    elif args.pep:
        args.short, args.flexible, args.medium, args.long = \
            False, True, False, False
        args.patch_prefix = ".post"

    version = None
    versionArgs = {
        "patchPrefix": args.patch_prefix,
        "hashPrefix": args.hash_prefix}

    if args.test:
        verify_version_file()
    elif args.short:
        version = get_version(SHORT, **versionArgs)
    elif args.medium:
        version = get_version(MEDIUM, **versionArgs)
    elif args.long:
        version = get_version(LONG, **versionArgs)
    elif args.commit:
        print(get_commit_hash())
    elif args.bump:
        bump_version(*args.bump)
    else:  # either args.flexible or none
        version = get_version(MEDIUM_OR_SHORT, **versionArgs)

    if version is not None:
        print(args.prefix + version)
