{# THEMODULE #}
{{ node.name }}
{{ '=' * node.name|length }}

.. automodule:: {{ node.name }}
  :no-special-members:

{# SET CURRENT MODULE #}
.. currentmodule:: {{ node.name }}
{# CONTENT #}
{%- block content -%}
{%- if node.exceptions or node.classes or node.functions or node.variables %}

Outline
-------

{%- if node.classes %}

Classes
^^^^^^^

.. list-table::
  :widths: 25 75
{% for item, obj in node.classes.items() %}
  * - :py:class:`{{ item }}`
    - {{ obj|summary }}
{%- endfor -%}
{%- endif -%}
{%- if node.functions %}

Functions
^^^^^^^^^

.. list-table::
  :widths: 25 75
{% for item, obj in node.functions.items() %}
  * - :py:func:`{{ item }}`
    - {{ obj|summary }}
{%- endfor -%}
{%- endif -%}
{% if node.exceptions %}

Exceptions
^^^^^^^^^^

.. list-table::
  :widths: 25 75
{% for item, obj in node.exceptions.items() %}
  * - :py:exc:`{{ item }}`
    - {{ obj|summary }}
{%- endfor -%}
{%- endif -%}
{%- if node.variables %}

Data
^^^^

.. hlist::
  :columns: 4

{% for item, obj in node.variables.items() %}
  - :py:data:`{{ item }}`
{%- endfor -%}
{%- endif -%}
{%- endif %}
{%- if subnodes %}

Submodules
^^^^^^^^^^

.. list-table::
  :widths: 25 75
{% for subnode in subnodes %}
  * - :py:mod:`{{ subnode.name }}`
    - {{ subnode.module|summary }}
{%- endfor %}
{%- endif -%}
{%- endblock -%}
{# CLASSES #}
{%- block classes -%}
{%- if node.classes %}

Classes
-------

{% for item in node.classes %}
{{ item }}
{{ '^' * item|length }}

.. autoclass:: {{ item }}
  :members:
  :undoc-members:
{% endfor -%}
{%- endif -%}
{%- endblock -%}
{# FUNCTIONS #}
{%- block functions -%}
{%- if node.functions %}

Functions
---------

{% for item in node.functions %}
{{ item }}
{{ '^' * item|length }}

.. autofunction:: {{ item }}
{% endfor -%}
{%- endif -%}
{%- endblock -%}
{# EXCEPTIONS #}
{%- block exceptions -%}
{%- if node.exceptions %}

Exceptions
----------

{% for item in node.exceptions %}
{{ item }}
{{ '^' * item|length }}

.. autoexception:: {{ item }}
{% endfor -%}
{%- endif -%}
{%- endblock -%}
{# DATA #}
{%- block variables -%}
{%- if node.variables %}

Data
----

{% for item, obj in node.variables.items() %}

.. autodata:: {{ item }}
  :annotation:
{% endfor -%}
{%- endif -%}
{%- endblock -%}
