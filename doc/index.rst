.. figure:: picos_trans.png

.. _welcome:

The PICOS Documentation
=======================

Welcome to the documentation of PICOS, a user friendly Python API to several
conic and integer programming solvers, whose open source code lives on
`GitLab <https://gitlab.com/picos-api/picos>`_.

PDF versions of the
`full documentation <https://gitlab.com/picos-api/picos/-/jobs/artifacts/master/raw/picos.pdf?job=pdfdoc>`_
and only the
`API reference <https://gitlab.com/picos-api/picos/-/jobs/artifacts/master/raw/picos-api.pdf?job=pdfdoc>`_
are available for offline use.


.. _quickstart:

Quickstart guide
----------------

- If you are **new** to PICOS, the :ref:`introduction section <introduction>`
  lists features and installation instructions.
- If you want to know what has changed with the **latest release**, head to the
  :ref:`changelog <changelog>`.
- The :ref:`API reference <api>` details all classes and functions that PICOS
  offers to your program.
- If you want to report a **bug** or propose a feature, the
  :ref:`contribution guide <contributing>` has you covered.
- If you still have a **question**, we're happy to receive
  `your mail <incoming+picos-api/picos@incoming.gitlab.com>`_!


.. _contents:

Documentation outline
---------------------

.. toctree::
   :maxdepth: 2

   Introduction <introduction>
   userguide
   api
   contributing
   changelog
