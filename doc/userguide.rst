.. _userguide:

User's Guide
============

If you are new to PICOS, you can start with the :ref:`tutorial <tutorial>` or
dig into our :ref:`examples <examples>`. As an experienced user, you can refer
to the :ref:`function cheat sheet <cheatsheet>` or go directly to the
:ref:`API reference <api>`.

.. toctree::
   :maxdepth: 2

   tutorial.rst
   constraints.rst
   examples.rst
   cheatsheet.rst
   slicing.rst
   duals.rst
   tolerances.rst
