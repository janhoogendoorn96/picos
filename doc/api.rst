.. _api:

API Reference
=============

PICOS is organized in a number of submodules and subpackages, most of which you
do not need to access directly when solving optimization problems. It is usually
sufficient to ``import picos`` and use the functions and classes provided in the
:mod:`picos` namespace.

.. toctree::
  :maxdepth: 1

  api/picos
  api/picos.apidoc
  api/picos.caching
  api/picos.compat
  api/picos.constraints
  api/picos.containers
  api/picos.expressions
  api/picos.formatting
  api/picos.glyphs
  api/picos.legacy
  api/picos.modeling
  api/picos.reforms
  api/picos.solvers
  api/picos.tools

More
----

Modules below the second level are not listed here but can be found in the
sidebar to your left. Often these modules define a single class which is also
mirrored in one of the namespaces above.

.. toctree::
  :hidden:
  :glob:

  api/picos.*.*
