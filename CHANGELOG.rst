Changelog
=========

This file documents major changes to PICOS. The format is based on
`Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_.

.. _Unreleased: https://gitlab.com/picos-api/picos/compare/v2.0...master
.. _2.0: https://gitlab.com/picos-api/picos/compare/v1.2.0...v2.0
.. _1.2.0: https://gitlab.com/picos-api/picos/compare/v1.1.3...v1.2.0
.. _1.1.3: https://gitlab.com/picos-api/picos/compare/v1.1.2...v1.1.3
.. _1.1.2: https://gitlab.com/picos-api/picos/compare/v1.1.1...v1.1.2
.. _1.1.1: https://gitlab.com/picos-api/picos/compare/v1.1.0...v1.1.1
.. _1.1.0: https://gitlab.com/picos-api/picos/compare/v1.0.2...v1.1.0
.. _1.0.2: https://gitlab.com/picos-api/picos/compare/v1.0.1...v1.0.2
.. _1.0.1: https://gitlab.com/picos-api/picos/compare/v1.0.0...v1.0.1
.. _1.0.0: https://gitlab.com/picos-api/picos/compare/b65a05be...v1.0.0
.. _0.1.3: about:blank
.. _0.1.2: about:blank
.. _0.1.1: about:blank
.. _0.1.0: about:blank


`Unreleased`_
--------------------------------------------------------------------------------

.. rubric:: Fixed

- Quadratic expressions created from a squared norm failing to decompose into
  a squared norm due to a numerically singular quadratic form (speculative fix).
- Solution objects unintendedly sharing memory.


`2.0`_ - 2020-03-03
--------------------------------------------------------------------------------

.. rubric:: Important

This is a major release featuring vast backend rewrites as well as interface
changes. Programs written for older versions of PICOS are expected to raise
deprecation warnings but should otherwise work as before. The following lists
notable exceptions:

- The solution returned by :meth:`~picos.Problem.solve` is now an instance of
  the new :class:`~picos.Solution` class instead of a dictionary.
- If solution search fails to find an optimal primal solution, PICOS will now
  raise a :class:`~picos.SolutionFailure` by default. Old behavior of not
  raising an exception is achieved by setting ``primals=None`` (see
  :ref:`primals <option_primals>` and :ref:`duals <option_duals>` options).
- The definition of the :math:`L_{p,q}`-norm has changed: It no longer refers
  to the :math:`p`-norm of the :math:`q`-norms of the matrix rows but to the
  :math:`q`-norm of the :math:`p`-norms of the matrix columns. This matches
  the definition you would find
  `on Wikipedia <https://en.wikipedia.org/wiki/Matrix_norm#L2,1_and_Lp,q_norms>`_
  and should reduce confusion for new users. See :class:`~picos.Norm`.
- The signs in the Lagrange dual problem of a conic problem are now more
  consistent for all cones, see :ref:`duals`. In particular the signs of dual
  values for (rotated) second order conic constraints have changed and the
  problem obtained by :attr:`Problem.dual <picos.Problem.dual>` (new for
  :meth:`~picos.Problem.as_dual`) has a different (but equivalent) form.

.. rubric:: Added

- A modular problem reformulation framework. Before selecting a solver, PICOS
  now builds a map of problem types that your problem can be reformulated to
  and makes a choice based on the expected complexity of the reposed problem.
- An object oriented interface to solution search options. See
  :class:`~picos.Options`.
- Support for arbitrary objective functions via an epigraph reformulation.
- Support for MOSEK 9.
- Support for ECOS 2.0.7.
- Support for multiple subsystems with :func:`~picos.partial_trace`.
- Quick-solve functions :func:`picos.minimize` and :func:`picos.maximize`.
- Lower and upper diagonal matrix variable types.
- :class:`~picos.SecondOrderCone` and :class:`~picos.RotatedSecondOrderCone`
  sets to explicitly create the associated constraints. *(You now need to use
  these if you want to obtain a conic instead of a quadratic dual.)*
- Possibility to use :func:`picos.sum` to sum over the elements of a single
  multidimensional expression.
- Possibility to create a :class:`~picos.Ball` or :class:`~picos.Simplex` with a
  non-constant radius.
- Many new properties (postfix operations) to work with affine expressions; for
  instance ``A.vec`` is a faster and cached way to express the vectorization
  ``A[:]``. See :class:`~picos.expressions.ComplexAffineExpression`.
- Options :ref:`assume_conic <option_assume_conic>` and
  :ref:`verify_prediction <option_verify_prediction>`.
- An option for every solver to manipulate the chances of it being selected
  (e.g. :ref:`penalty_cvxopt <option_penalty_cvxopt>`).
- Ability to run doctests via ``test.py``.

.. rubric:: Fixed

The following are issues that were fixed in an effort of their own. If a bug is
not listed here, it might still be fixed as a side effect of some of the large
scale code rewrites that this release ships.

- Upgrading the PyPI package via pip.
- A regression that rendered the Kronecker product unusable.
- Noisy exception handling in a sparse matrix helper function.
- Shape detection for matrices given by string.
- The :ref:`hotstart <option_hotstart>` option when solving with CPLEX.
- Low precision QCP duals from Gurobi.

.. rubric:: Changed

- All algebraic expression code has been rewritten and organized in a new
  :mod:`~picos.expressions` package. In particular, real and complex expressions
  are distinguished more clearly.
- All algebraic expressions are now immutable.
- The result of any unary operation on algebraic expressions (e.g. negation,
  transposition) is cached (only computed once per expression).
- Slicing of affine expressions is more powerful, see :ref:`slicing`.
- Loading of constant numeric data has been unified, see
  :func:`~picos.expressions.data.load_data`.
- Variables are now created independently of problems by instanciating one of
  the new :mod:`variable types <picos.expressions.variables>`.
  *(*:meth:`Problem.add_variable <picos.Problem.add_variable>` *is deprecated.)*
- Constraints are added to problems as they are; any transformation is done
  transparently during solution search.
- In particular, :math:`x^2 \leq yz` is now initially a (nonconvex) quadratic
  constraint and transformation to a conic constraint is controlled by the new
  :ref:`assume_conic <option_assume_conic>` option.
- Expressions constrained to be positive semidefinite are now required to be
  symmetric/hermitian by their own definition. *(Use*
  :class:`~picos.SymmetricVariable` *or* :class:`~picos.HermitianVariable`
  *whenever applicable!)*
- Options passed to :meth:`~picos.Problem.solve` are only used for that
  particular search.
- The default value for the :ref:`verbosity <option_verbosity>` option (formerly
  ``verbose``) is now :math:`0`.
- Available solvers are only imported when they are actually being used, which
  speeds up import of PICOS on platforms with many solvers installed.
- The code obeys PEP 8 and PEP 257 more strongly. Exceptions: D105, D203, D213,
  D401, E122, E128, E221, E271, E272, E501, E702, E741.
- Production testing code was moved out of the :mod:`picos` package.

.. rubric:: Removed

- The ``NoAppropriateSolverError`` exception that was previously raised by
  :meth:`~picos.Problem.solve`. This is replaced by the new
  :class:`~picos.SolutionFailure` exception with error code :math:`1`.
- Some public functions in the :mod:`~picos.tools` module that were originally
  meant for internal use.

.. rubric:: Deprecated

This section lists deprecated modules, functions and options with their
respective replacement or deprecation reason on the right hand side.
Deprecated entities produce a warning and will be removed in a future release.

- The :mod:`~picos.tools` module as a whole. It previously contained both
  algebraic functions for the user as well as functions meant for internal use.
  The former group of functions can now be imported directly from the
  :mod:`picos` namespace (though some are also individually deprecated). The
  other functions were either relocated (but can still be imported from
  :mod:`~picos.tools` while it lasts) or removed.
- In the :class:`~picos.Problem` class:

  - :meth:`~picos.Problem.add_variable`,
    :meth:`~picos.Problem.remove_variable`,
    :meth:`~picos.Problem.set_var_value`
    → variables are instanciated directly and added to problems automatically
  - :meth:`~picos.Problem.minimize` → :func:`picos.minimize`
  - :meth:`~picos.Problem.maximize` → :func:`picos.maximize`
  - :meth:`~picos.Problem.set_option`
    → assign to attributes or items of :attr:`Problem.options <picos.Options>`
  - :meth:`~picos.Problem.update_options`
    → :meth:`options.update <picos.Options.update>`
  - :meth:`~picos.Problem.set_all_options_to_default`
    → :meth:`options.reset <picos.Options.reset>`
  - :meth:`~picos.Problem.obj_value` → :attr:`~picos.Problem.value`
  - :meth:`~picos.Problem.is_continuous` → :attr:`~picos.Problem.continuous`
  - :meth:`~picos.Problem.is_pure_integer` → :attr:`~picos.Problem.pure_integer`
  - :meth:`~picos.Problem.verbosity`
    → :ref:`options.verbosity <option_verbosity>`
  - :meth:`~picos.Problem.as_dual` → :attr:`~picos.Problem.dual`
  - :meth:`~picos.Problem.countVar`,
    :meth:`~picos.Problem.countCons`,
    :meth:`~picos.Problem.numberOfVars`,
    :meth:`~picos.Problem.numberLSEConstraints`,
    :meth:`~picos.Problem.numberSDPConstraints`,
    :meth:`~picos.Problem.numberQuadConstraints`,
    :meth:`~picos.Problem.numberConeConstraints`
    → were meant for internal use
  - arguments ``it``, ``indices`` and ``key`` to
    :meth:`~picos.Problem.add_list_of_constraints` → are ignored

- All expression types:

  - constraint creation via ``<`` → ``<=``
  - constraint creation via ``>`` → ``>=``
  - :meth:`~picos.expressions.Expression.is_valued`
    → :attr:`~picos.expressions.Expression.valued`
  - :meth:`~picos.expressions.Expression.set_value`
    → assign to :attr:`~picos.expressions.Expression.value`

- Affine expressions:

  - :meth:`~picos.expressions.ComplexAffineExpression.fromScalar`
    → :meth:`~picos.expressions.ComplexAffineExpression.from_constant`
    or :func:`picos.Constant`
  - :meth:`~picos.expressions.ComplexAffineExpression.fromMatrix`
    → :meth:`~picos.expressions.ComplexAffineExpression.from_constant`
    or :func:`picos.Constant`
  - :meth:`~picos.expressions.ComplexAffineExpression.hadamard` → ``^``
  - :meth:`~picos.expressions.ComplexAffineExpression.isconstant`
    → :meth:`~picos.expressions.ComplexAffineExpression.constant`
  - :meth:`~picos.expressions.ComplexAffineExpression.same_as`
    → :meth:`~picos.expressions.ComplexAffineExpression.equals`
  - :meth:`~picos.expressions.ComplexAffineExpression.transpose`
    → :attr:`~picos.expressions.ComplexAffineExpression.T`
  - :attr:`~picos.expressions.ComplexAffineExpression.Tx`
    → :meth:`~picos.expressions.ComplexAffineExpression.partial_transpose`
  - :meth:`~picos.expressions.ComplexAffineExpression.conjugate`
    → :attr:`~picos.expressions.ComplexAffineExpression.conj`
  - :meth:`~picos.expressions.ComplexAffineExpression.Htranspose`
    → :attr:`~picos.expressions.ComplexAffineExpression.H`
  - :meth:`~picos.expressions.ComplexAffineExpression.copy`
    → expressions are immutable
  - :meth:`~picos.expressions.ComplexAffineExpression.soft_copy`
    → expressions are immutable

- Algebraic functions and shorthands in the ``picos`` namespace:

  - :func:`~picos.tracepow` → :class:`~picos.PowerTrace`
  - :func:`~picos.new_param` → :func:`~picos.Constant`
  - :func:`~picos.flow_Constraint` → :class:`~picos.FlowConstraint`
  - :func:`~picos.diag_vect` → :func:`~picos.maindiag`
  - :func:`~picos.simplex` → :class:`~picos.Simplex`
  - :func:`~picos.truncated_simplex` → :class:`~picos.Simplex`
  - arguments ``it`` and ``indices`` to :func:`~picos.sum` → are ignored

- Solution search options:

  - ``allow_license_warnings``
    → :ref:`license_warnings <option_license_warnings>`
  - ``verbose`` → :ref:`verbosity <option_verbosity>` (takes an integer)
  - ``noprimals`` → :ref:`primals <option_primals>` (the meaning is inverted)
  - ``noduals`` → :ref:`duals <option_duals>` (the meaning is inverted)
  - ``tol`` →  ``*_fsb_tol`` and ``*_ipm_opt_tol``
  - ``gaplim`` → :ref:`rel_bnb_opt_tol <option_rel_bnb_opt_tol>`
  - ``maxit`` → :ref:`max_iterations <option_max_iterations>`
  - ``nbsol`` → :ref:`max_fsb_nodes <option_max_fsb_nodes>`
  - ``pool_relgap`` → :ref:`pool_rel_gap <option_pool_rel_gap>`
  - ``pool_absgap`` → :ref:`pool_abs_gap <option_pool_abs_gap>`
  - ``lboundlimit`` → :ref:`cplex_lwr_bnd_limit <option_cplex_lwr_bnd_limit>`
  - ``uboundlimit`` → :ref:`cplex_upr_bnd_limit <option_cplex_upr_bnd_limit>`
  - ``boundMonitor`` → :ref:`cplex_bnd_monitor <option_cplex_bnd_monitor>`
  - ``solve_via_dual`` → :ref:`dualize <option_dualize>` (may not be :obj:`None`
    any more)


`1.2.0`_ - 2019-01-11
--------------------------------------------------------------------------------

.. rubric:: Important

- :attr:`A scalar expression's value <picos.expressions.Expression.value>` and
  :attr:`a scalar constraint's dual <picos.constraints.Constraint.dual>` are
  returned as scalar types as opposed to 1×1 matrices.
- The dual value returned for rotated second order cone constraints is now a
  proper member of the dual cone (which equals the primal cone up to a factor of
  :math:`4`). Previously, the dual of an equivalent second order cone constraint
  was returned.
- The Python 2/3 compatibility library ``six`` is no longer a dependency.

.. rubric:: Added

- Support for the ECOS solver.
- Experimental support for MOSEK's new Fusion API.
- Full support for exponential cone programming.
- A production testing framework featuring around 40 novel optimization test
  cases that allows quick selection of tests, solvers, and solver options.
- A "glyph" system that allows the user to adjust the string representations of
  future expressions and constraints. For instance,
  :func:`picos.latin1() <picos.glyphs.latin1>` disables use of unicode symbols.
- Support for symmetric variables with all solvers, even if they do not support
  semidefinite programming.

.. rubric:: Changed

- Solver implementations each have a source file of their own, and a common
  interface that makes implementing new solvers easier.
- Likewise, constraint implementations each have a source file of their own.
- The implementations of CPLEX, Gurobi, MOSEK and SCIP have been rewritten.
- Solver selection takes into account how well a problem is supported,
  distinguishing between native, secondary, experimental and limited support.
- Unsupported operations on expressions now produce meaningful exceptions.
- :func:`add_constraint <picos.Problem.add_constraint>` and
  :func:`add_list_of_constraints <picos.Problem.add_list_of_constraints>`
  always return the constraints
  passed to them.
- :func:`add_list_of_constraints <picos.Problem.add_list_of_constraints>`
  and :func:`picos.sum` find a short string representation automatically.

.. rubric:: Removed

- The old production testing script.
- Support for the SDPA solver.
- Support for sequential quadratic programming.
- The options ``convert_quad_to_socp_if_needed``, ``pass_simple_cons_as_bound``,
  ``return_constraints``, ``handleBarVars``, ``handleConeVars`` and
  ``smcp_feas``.
- Support for GLPK and MOSEK through CVXOPT.

.. rubric:: Fixed

- Performance issues when exporting variable bounds to CVXOPT.
- Hadamard product involving complex matrices.
- Adding constant terms to quadratic expression.
- Incorrect or redundant expression string representations.
- GLPK handling of the default ``maxit`` option.
- Miscellaneous solver-specific bugs in the solvers that were re-implemented.


`1.1.3`_ - 2018-10-05
--------------------------------------------------------------------------------

.. rubric:: Added

- Support for the solvers GLPK and SCIP.
- PICOS packages `on Anaconda Cloud <https://anaconda.org/picos/picos>`_.
- PICOS packages
  `in the Arch Linux User Repository <https://aur.archlinux.org/packages/?SeB=b&K=python-picos>`_.

.. rubric:: Changed

- The main repository has moved to `GitLab <https://gitlab.com/picos-api/picos>`_.
- Releases of packages and documentation changes are
  `automated <https://about.gitlab.com/features/gitlab-ci-cd/>`_ and thus more
  frequent. In particular, post release versions are available.
- Test bench execution is automated for greater code stability.
- Improved test bench output.
- Improved support for the SDPA solver.
- :func:`~picos.partial_trace` can handle rectangular subsystems.
- The documentation was restructured; examples were converted to Python 3.

.. rubric:: Fixed

- Upper bounding the norm of a complex scalar.
- Multiplication with a complex scalar.
- A couple of Python 3 specific errors, in particular when deleting constraints.
- All documentation examples are reproducible with the current state of PICOS.


`1.1.2`_ - 2016-07-04
--------------------------------------------------------------------------------

.. rubric:: Added

- Ability to dynamically add and remove constraints.
- Option ``pass_simple_cons_as_bound``, see below.

.. rubric:: Changed

- Improved efficiency when processing large expressions.
- Improved support for the SDPA solver.
- :func:`add_constraint <picos.Problem.add_constraint>` returns a handle to the
  constraint when the option `return_constraints` is set.
- New signature for the function :func:`~picos.partial_transpose`, which can now
  transpose arbitrary subsystems from a kronecker product.
- PICOS no longer turns constraints into variable bounds, unless the new option
  ``pass_simple_cons_as_bound`` is enabled.

.. rubric:: Fixed

- Minor bugs with complex expressions.


`1.1.1`_ - 2015-08-29
--------------------------------------------------------------------------------

.. rubric:: Added

- Support for the SDPA solver.
- Partial trace of an affine expression, see
  :func:`~picos.partial_trace`.

.. rubric:: Changed

- Improved PEP 8 compliance.

.. rubric:: Fixed

- Compatibility with Python 3.


`1.1.0`_ - 2015-04-15
--------------------------------------------------------------------------------

.. rubric:: Added

- Compatibility with Python 3.

.. rubric:: Changed

- The main repository has moved to `GitHub <https://github.com/gsagnol/picos>`_.


`1.0.2`_ - 2015-01-30
--------------------------------------------------------------------------------

.. rubric:: Added

- Ability to read and write problems in
  `conic benchmark format <http://cblib.zib.de/>`_.
- Support for inequalities involving the sum of the :math:`k` largest or
  smallest elements of an affine expression, see :func:`~picos.sum_k_largest`
  and :func:`~picos.sum_k_smallest`.
- Support for inequalities involving the sum of the :math:`k` largest or
  smallest eigenvalues of a symmetric matrix, see
  :func:`~picos.sum_k_largest_lambda`, :func:`~picos.sum_k_smallest_lambda`,
  :func:`~picos.lambda_max` and :func:`~picos.lambda_min`.
- Support for inequalities involving the :math:`L_{p,q}`-norm of an affine
  expression, see :func:`~picos.norm`.
- Support for equalities involving complex coefficients.
- Support for antisymmetric matrix variables.
- Set expressions that affine expressions can be constrained to be an element
  of, see :func:`~picos.ball`, :func:`~picos.simplex` and
  :func:`~picos.truncated_simplex`.
- Shorthand functions :func:`maximize <picos.Problem.maximize>` and
  :func:`minimize <picos.Problem.minimize>` to specify the objective function of
  a problem and solve it.
- Hadamard (elementwise) product of affine expression, as an overload of the
  ``^`` operator, read :ref:`the tutorial on overloads <overloads>`.
- Partial transposition of an aAffine Expression, see
  :func:`~picos.partial_transpose`.

.. rubric:: Changed

- Improved efficiency of the sparse SDPA file format writer.
- Improved efficiency of the complex to real transformation.

.. rubric:: Fixed

- Scalar product of hermitian matrices.
- Conjugate of a complex expression.


`1.0.1`_ - 2014-08-27
--------------------------------------------------------------------------------

.. rubric:: Added

- Support for semidefinite programming over the complex domain, see
  :ref:`the documentation on complex problems <complex>`.
- Helper function to input (multicommodity) graph flow problems, see
  :ref:`the tutorial on flow constraints <flowcons>`.
- Additional argument to :func:`~picos.tracepow`, to represent constraints
  of the form :math:`\operatorname{trace}(M X^p) \geq t`.

.. rubric:: Changed

- Significantly improved slicing performance for affine expressions.
- Improved performance when loading data.
- Improved performance when retrieving primal solution from CPLEX.
- The documentation received an overhaul.


`1.0.0`_ - 2013-07-19
--------------------------------------------------------------------------------

.. rubric:: Added

- Ability to express rational powers of affine expressions with the ``**``
  operator, traces of matrix powers with :func:`~picos.tracepow`,
  (generalized) p-norms with :func:`~picos.norm` and :math:`n`-th roots of a
  determinant with :func:`~picos.detrootn`.
- Ability to specify variable bounds directly rather than by adding constraints,
  see :func:`add_variable <picos.Problem.add_variable>`.
- Problem dualization.
- Option ``solve_via_dual`` which controls passing the dual problem to the
  solver instead of the primal problem. This can result in a significant
  speedup for certain problems.
- Semidefinite programming interface for MOSEK 7.0.
- Options ``handleBarVars`` and ``handleConeVars`` to customize how SOCPs and
  SDPs are passed to MOSEK. When these are set to ``True``, PICOS tries to
  minimize the number of variables of the MOSEK instance.

.. rubric:: Changed

- If the chosen solver supports this, updated problems will be partially
  re-solved instead of solved from scratch.

.. rubric:: Removed

- Option ``onlyChangeObjective``.


`0.1.3`_ - 2013-04-17
--------------------------------------------------------------------------------

.. rubric:: Added

- A :func:`~picos.geomean` function to construct geometric mean inequalities
  that will be cast as rotated second order cone constraints.
- Options ``uboundlimit`` and ``lboundlimit`` to tell CPLEX to stop the search
  as soon as the given threshold is reached for the upper and lower bound,
  respectively.
- Option ``boundMonitor`` to inspect the evolution of CPLEX lower and upper
  bounds.
- Ability to use the weak inequality operators as an alias for the strong ones.

.. rubric:: Changed

- The solver search time is returned in the dictionary returned by
  :func:`solve <picos.Problem.solve>`.

.. rubric:: Fixed

- Access to dual values of fixed variables with CPLEX.
- Evaluation of constant affine expressions with a zero coefficient.
- Number of constraints not being updated in
  :func:`remove_constraint <picos.Problem.remove_constraint>`.


`0.1.2`_ - 2013-01-10
--------------------------------------------------------------------------------

.. rubric:: Fixed

- Writing SDPA files. The lower triangular part of the constraint matrix was
  written instead of the upper triangular part.
- A wrongly raised :class:`IndexError` from
  :func:`remove_constraint <picos.Problem.remove_constraint>`.


`0.1.1`_ - 2012-12-08
--------------------------------------------------------------------------------

.. rubric:: Added

- Interface to Gurobi.
- Ability to give an initial solution to warm-start mixed integer optimizers.
- Ability to get a reference to a constraint that was added.

.. rubric:: Fixed

- Minor bugs with quadratic expressions.


`0.1.0`_ - 2012-06-22
--------------------------------------------------------------------------------

.. rubric:: Added

- Initial release of PICOS.
